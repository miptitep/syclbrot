#include "FracGenWindow.hpp"
#include "FracGen.hpp"
#include <cfloat>
#include <future>
#include <iostream>
#include <limits>
#include <thread>

FracGenWindow::FracGenWindow(int w, int h, std::string &flavourDesc,
                             std::shared_ptr<bool> redrawFlag,
                             std::shared_ptr<bool> exit)
    : width{w}, height{h}, colourDepth{32}, drawRect{false}, HL{0, 0, 0, 0},
      mouseX{0}, mouseY{0}, redrawRequired{redrawFlag}, exitNow{exit} {
  SDL_Init(SDL_INIT_EVERYTHING);
  std::string title = flavourDesc + " Toybrot Mandelbox";
  mainwindow = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED,
                                SDL_WINDOWPOS_UNDEFINED, width, height,
                                SDL_WINDOW_SHOWN);

  render = SDL_CreateRenderer(mainwindow, -1, 0);

  screen =
      SurfPtr(SDL_CreateRGBSurface(0, width, height, colourDepth, 0xFF000000,
                                   0x00FF0000, 0x0000FF00, 0x000000FF));

  texture = SDL_CreateTexture(render, SDL_PIXELFORMAT_RGBA8888,
                              SDL_TEXTUREACCESS_STREAMING, width, height);

  surface =
      SurfPtr(SDL_CreateRGBSurface(0, width, height, colourDepth, 0xFF000000,
                                   0x00FF0000, 0x0000FF00, 0x000000FF));

  highlight = SurfUnq(SDL_CreateRGBSurface(0, w, h, colourDepth, 0xFF000000,
                                           0x00FF0000, 0x0000FF00, 0x000000FF));

  SDL_SetSurfaceBlendMode(highlight.get(), SDL_BLENDMODE_BLEND);
  void *pix = highlight->pixels;
  for (int i = 0; i < surface->h; i++) {
    for (int j = 0; j < surface->w; j++) {

      auto p = reinterpret_cast<uint32_t *>(pix) + (i * highlight->w) + j;
      *p = SDL_MapRGB(surface->format, 255u, 255u, 255u);
    }
  }
  SDL_SetSurfaceAlphaMod(highlight.get(), 128u);
}

FracGenWindow::~FracGenWindow() {
  SDL_DestroyWindow(mainwindow);
  SDL_DestroyRenderer(render);
  SDL_DestroyTexture(texture);
}

void FracGenWindow::paint() {
  SDL_BlitSurface(surface.get(), nullptr, screen.get(), nullptr);
  //    if(drawRect)
  //    {
  //        drawHighlight();
  //    }

  SDL_UpdateTexture(texture, nullptr, screen->pixels, screen->pitch);
  SDL_RenderClear(render);
  SDL_RenderCopy(render, texture, nullptr, nullptr);
  SDL_RenderPresent(render);
}

bool FracGenWindow::captureEvents() {
  SDL_Event event;
  while (SDL_PollEvent(&event)) {
    switch (event.type) {
    case SDL_KEYDOWN:
    case SDL_KEYUP:
      return onKeyboardEvent(event.key);

    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
      break;
      //            if(*redrawRequired)
      //            {
      //                return true;
      //            }
      //            else
      //            {
      //                return onMouseButtonEvent(event.button);
      //            }
    case SDL_QUIT:
      *exitNow = true;
      return false;

    case SDL_MOUSEMOTION:
      //            if(*redrawRequired)
      //            {
      //                return true;
      //            }
      //            else
      //            {
      //                return onMouseMotionEvent(event.motion);
      //            }
    case SDL_JOYAXISMOTION:
    case SDL_JOYBUTTONDOWN:
    case SDL_JOYBUTTONUP:
    case SDL_JOYHATMOTION:
    case SDL_JOYBALLMOTION:
      //		case SDL_ACTIVEEVENT:
      //		case SDL_VIDEOEXPOSE:
      //		case SDL_VIDEORESIZE:
      break;

    default:
      // Unexpected event type!
      // assert(0);
      break;
    }
  }
  std::this_thread::sleep_for(std::chrono::milliseconds(30));
  return true;
}

void FracGenWindow::setTitle(std::string title) {
  SDL_SetWindowTitle(mainwindow, title.c_str());
}

void FracGenWindow::drawHighlight() {
  SDL_Rect r;
  r.x = (HL.x < mouseX ? HL.x : mouseX);
  r.y = (HL.y < mouseY ? HL.y : mouseY);
  r.w = abs(mouseX - HL.x);
  r.h = abs(mouseY - HL.y);
  SDL_BlitSurface(highlight.get(), &r, screen.get(), &r);
}

void FracGenWindow::updateFractal() {
  if (fractal == nullptr) {
    return;
  }
  if (fractal->size() != static_cast<size_t>(width * height)) {
    std::cout << "Fractal and SDL_Surface length mismatch!" << std::endl;
    exit(2);
  }

  /*
   * multi-threading the conversion to the SDL type
   * using similar logic/mechanisms to the STD::TASKS
   * version of the generation itself
   */
  std::vector<std::future<void>> tasks(std::thread::hardware_concurrency());
  SDL_LockSurface(surface.get());
  // "classic" for loop because we need to know the idx here
  for (size_t idx = 0; idx < std::thread::hardware_concurrency(); idx++) {
    tasks[idx] = std::async([this, idx]() { this->convertPixels(idx); });
  }
  for (auto &t : tasks) {
    t.get();
  }

  SDL_UnlockSurface(surface.get());
}

void FracGenWindow::convertPixels(size_t idx) {
  static size_t numTasks = std::thread::hardware_concurrency();
  size_t surfLength = width * height;
  for (size_t i = idx; i < surfLength; i += numTasks) {
    Vec4<uint8_t> colour8(
        static_cast<uint8_t>((*fractal)[i].R() *
                             std::numeric_limits<uint8_t>::max()),
        static_cast<uint8_t>((*fractal)[i].G() *
                             std::numeric_limits<uint8_t>::max()),
        static_cast<uint8_t>((*fractal)[i].B() *
                             std::numeric_limits<uint8_t>::max()),
        static_cast<uint8_t>((*fractal)[i].A() *
                             std::numeric_limits<uint8_t>::max()));

    reinterpret_cast<uint32_t *>(surface->pixels)[i] = SDL_MapRGBA(
        surface->format, colour8.R(), colour8.G(), colour8.B(), colour8.A());
  }
}

/******************************************************************************
 *
 * SDL event handling below here
 *
 *****************************************************************************/

bool FracGenWindow::onKeyboardEvent(const SDL_KeyboardEvent &e) noexcept {
  if (e.type == SDL_KEYDOWN) {
    if (e.keysym.sym == SDLK_ESCAPE) {
      *exitNow = true;
      return false;
    }
  }
  return true;
}

bool FracGenWindow::onMouseMotionEvent(const SDL_MouseMotionEvent &e) noexcept {
  mouseX = e.x;
  mouseY = e.y;
  int rw = mouseX - HL.x;
  int rh = mouseY - HL.y;
  if (rh == 0) {
    return true;
  }
  double ra = abs(rw / rh);
  if (ra < cam->AR) {

    mouseX = HL.x + static_cast<int>(rh * cam->AR);

  } else {
    mouseY = HL.x + static_cast<int>(rw / cam->AR);
  }
  return true;
}

bool FracGenWindow::onMouseButtonEvent(const SDL_MouseButtonEvent &e) noexcept {
  if (e.button == 4) {
    // M_WHEEL_UP
    // std::cout<< "button 4" <<std::endl;
    return true;
  }
  if (e.button == 5) {
    // M_WHEEL_DOWN
    // std::cout<< "button 5" <<std::endl;
    return true;
  }
  if (e.button == 2) {
    // Middle Button
    if (e.type == SDL_MOUSEBUTTONDOWN) {
      if (colourScheme != nullptr) {
        *colourScheme = (*colourScheme) + 1;
        *redrawRequired = true;
      }
    }
    return true;
  }
  if (e.button == 3) {
    // Right Button
    return true;
  }
  if (e.button == 1) {
    if (e.type == SDL_MOUSEBUTTONDOWN) {
      HL.x = e.x;
      HL.y = e.y;
      HL.w = 0;
      HL.h = 0;
      drawRect = true;
    } else {

      drawRect = false;
      *redrawRequired = true;
    }
    return true;
  }
  return false;
}

double FracGenWindow::AspectRatio() const noexcept { return cam->AR; }

void FracGenWindow::setCamPos(Vec3f p) noexcept { cam->pos = p; }

void FracGenWindow::setCamTarget(Vec3f t) noexcept { cam->target = t; }

Vec3f FracGenWindow::CamPos() const noexcept { return cam->pos; }

Vec3f FracGenWindow::CamTarget() const noexcept { return cam->target; }

std::shared_ptr<Camera> FracGenWindow::Cam() noexcept { return cam; }
