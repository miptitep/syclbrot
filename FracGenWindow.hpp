#ifndef TOYBROT_FRACGENWINDOW_DEFINED
#define TOYBROT_FRACGENWINDOW_DEFINED

#ifdef WIN32
#include "SDL.h"
#include "SDL_opengl.h"
#include <Windows.h>
#else

#include "SDL2/SDL.h"
#include "SDL2/SDL_opengl.h"

#endif

#include <memory>

#include "FracGen.hpp"

// Defining some convenience types

using SurfPtr = std::shared_ptr<SDL_Surface>;
using SurfUnq = std::unique_ptr<SDL_Surface>;

class Camera;

class FracGenWindow {
public:
  FracGenWindow(int width, int height, std::string &flavourDesc,
                std::shared_ptr<bool> redrawFlag, std::shared_ptr<bool> exit);
  ~FracGenWindow();

  void draw(SurfPtr surface);
  void paint();
  double AspectRatio() const noexcept;
  bool captureEvents();
  void registerRedrawFlag(std::shared_ptr<bool> b) noexcept {
    redrawRequired = b;
  }
  void registerColourFlag(std::shared_ptr<int> i) noexcept { colourScheme = i; }
  SurfPtr getSurf() noexcept { return surface; }
  void setFractal(FracPtr f) noexcept { fractal = f; }
  void setTitle(std::string title);
  void setCamPos(Vec3f p) noexcept;
  void setCamTarget(Vec3f t) noexcept;
  Vec3f CamPos() const noexcept;
  Vec3f CamTarget() const noexcept;
  std::shared_ptr<Camera> Cam() noexcept;
  void setCam(std::shared_ptr<Camera> c) { cam = c; }

  void updateFractal();

private:
  void drawHighlight();
  void convertPixels(size_t idx);

  bool onKeyboardEvent(const SDL_KeyboardEvent &e) noexcept;
  bool onMouseMotionEvent(const SDL_MouseMotionEvent &e) noexcept;
  bool onMouseButtonEvent(const SDL_MouseButtonEvent &e) noexcept;

  void resetHL(int x0, int y0);

  int width;
  int height;
  int colourDepth;
  bool drawRect;
  SDL_Rect HL;
  int mouseX;
  int mouseY;

  SDL_Window *mainwindow;
  SDL_Renderer *render;
  SurfPtr screen;
  SDL_Texture *texture;
  SurfPtr surface;
  SurfUnq highlight;

  std::string flavour;
  std::shared_ptr<bool> redrawRequired;
  std::shared_ptr<int> colourScheme;
  std::shared_ptr<bool> exitNow;
  std::shared_ptr<Camera> cam;
  FracPtr fractal;
};

#endif // TOYBROT_FRACGENWINDOW_DEFINED
