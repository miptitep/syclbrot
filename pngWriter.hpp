#ifndef PNGWRITER_H_DEFINED
#define PNGWRITER_H_DEFINED
#include <png.h>
#include <vector>
#include <cstdint>
#include <string>
#include "FracGen.hpp"

using pngColourType = png_uint_16;

struct pngRGBA
{
    pngColourType r;
    pngColourType g;
    pngColourType b;
    pngColourType a;
};

using pngData = std::vector<pngRGBA>;
using pngData2d = std::vector<pngData>;


class pngWriter
{
    public:

        pngWriter(uint32_t width = 0, uint32_t height = 0, std::string outname = "");

        void setWidth(unsigned int width)   noexcept {w = width;}
        void setHeight(unsigned int height) noexcept {h = height;}
        void setFractal(FracPtr f) noexcept;

        bool Init();
        bool Write();
        bool Write(const pngData& data);
        bool Write(const pngData2d& rows);
        void Alloc(pngData& data);
        void Alloc(pngData2d& rows);

    private:

        uint32_t w, h;

        png_byte bit_depth;
        png_structp writePtr;
        png_infop infoPtr;

        FracPtr fractal;
        pngData2d outData;

        std::string filename;
        std::string strippedName;
        std::string extensionName;
        std::string nameTweak;
        FILE* output;

};

#endif //PNGWRITER_H_DEFINED
